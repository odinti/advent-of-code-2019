// get input
const readline = require('readline');
const fs = require('fs');

function calculateModuleFuelRequirement (mass) {
    return Math.floor(mass / 3) - 2
}

const readInterface = readline.createInterface({
    input: fs.createReadStream('./input'),
    output: process.stdout,
    console: false
});

let sum = 0;

readInterface.on('line', function(line) {
    const mass = parseInt(line)
    sum = sum + calculateModuleFuelRequirement(mass)
});

readInterface.on('close', function (event) {
    console.log(sum)
})
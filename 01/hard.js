// get input
const readline = require('readline');
const fs = require('fs');

function calculateModuleFuelRequirement (mass) {
    const fuel = Math.floor(mass / 3) - 2;

    if (fuel < 0) {
        return 0;
    } else {
        return fuel + calculateModuleFuelRequirement(fuel);
    } 
}

const readInterface = readline.createInterface({
    input: fs.createReadStream('./input'),
    output: process.stdout,
    console: false
});

let sum = 0;

readInterface.on('line', function(line) {
    const mass = parseInt(line)
    sum = sum + calculateModuleFuelRequirement(mass)
});

readInterface.on('close', function (event) {
    console.log(sum)
})